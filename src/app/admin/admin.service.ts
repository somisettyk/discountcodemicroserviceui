import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AdminService {
  
  constructor(private http: HttpClient) { }

  getAdmins() {
    return this.http.get(window.location.protocol+"//"+ window.location.host+"/getRecipients");
  }

  addAdmin(recipient: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/addRecipient", recipient);    
  }

  deleteAdmin(recipientId: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/deleteRecipient?recipientId=" + recipientId, {});    
  }

  getOffers(): any {
    return this.http.get(window.location.protocol+"//"+ window.location.host+"/getSpecialOffers");
  }
  addOffer(offer: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/addSpecialOffer", offer);    
  }

  deleteOffer(offerId: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/deleteSpecialOffer?specialOfferId=" + offerId, {}); 
  }

  getDiscountCodesByEmail(emailId: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/getAllDiscountCodesByEmail?email=" + emailId, {}); 
  }

  redeemDiscountCode(disc: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/redeemDiscountCode", disc); 
  }

  validateDiscountCode(disc: any): any {
    return this.http.post(window.location.protocol+"//"+ window.location.host+"/validateAndGetDiscountCode", disc); 
  }
}